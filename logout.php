<?php
    include('_common.php');
    
    class Logout extends Template {
        function __construct() {
            parent::__construct();
            
            $this->setTitle('Logout');
            
            if ($this->context->isPost()) {
                logging('User '.$this->context->getCurrentUser()->EMail.' has logged out');
                $this->setMessage('Logout successful.');
                
                session_destroy();
            }
        }
        
        function help() {
            ?>
               <p>
                    Logout page.
                </p>
            <?php
        }
        
        function content() {
        }
    }
    
    $page = new Logout();
    $page->build();
?>