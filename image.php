<?php
    include('_common.php');
    
    class CommentPostData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('Content')->addValidationRule(new NotEmptyValidationRule());
        }
    }
    
    class CommentData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('Id');
            $this->defineField('Content');
            $this->defineField('Date');
            $this->defineField('UserName');
            $this->defineField('UserId');
            $this->defineField('UserProfilePictureId');
        }
    }
    
    class ImageViewData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('Id');
            $this->defineField('Title');
            $this->defineField('Category');
            $this->defineField('DateTaken');
            $this->defineField('DateUploaded');
            $this->defineField('Photographer');
            $this->defineField('Comments');
            $this->defineField('PictureId');
        }
    }
    
    class ImageView extends Template {
        private $image;
        private $id;
        private $img;
        
        function __construct() {   
            parent::__construct();
                     
            $this->id = $this->context->getParameter('id', '-1');
            $this->img = $this->context->repository->getImage($this->id);
            
            logging('Accessing image '.$this->id);
            
            if ($this->img == null) {
                $this->error('Image not found');
                return;
            }
            
            $this->processPostData();
            
            $category = $this->context->repository->getCategory($this->img->CategoryId);
            $user = $this->context->repository->getUser($this->img->UserId); 
            
            $this->setTitle(htmlspecialchars($this->img->Title));
            $this->image = new ImageViewData();
             
            $this->image->Id = $this->id;
            $this->image->Title = $this->img->Title;
            $this->image->Category = $category;
            $this->image->Photographer = $user;
            $this->image->DateTaken = formatDate($this->img->DateTaken, 'm/d/Y');
            $this->image->DateUploaded = formatDate($this->img->DateUploaded, 'm/d/Y');
            $this->image->PictureId = $this->img->PictureId;
            
            $comments = $this->context->repository->getCommentsForPicture($this->img->Id);
            $this->image->Comments = array_map(array($this, 'createComment'), $comments); 
        }
        
        function processPostData() {
            if (!$this->context->isPost())
                return;
                
            logging('Evaluating post data');
                
            if (isset($_POST['addComment']))
                $this->addComment();
            if (isset($_POST['delete']))
                $this->delete();
            if (isset($_POST['deleteComment']))
                $this->deleteComment($_POST['commentId']);
        }
        
        function delete() {
            logging('Trying to delete image '.$this->id);
            
            if (!$this->canDelete()) {
                logging('Image '.$this->id.' can not be deleted');
                
                $this->error('You cannot delete this image');
                return;
            }
            
            $this->context->repository->deleteImage($this->id);  
            $this->error('Image has been deleted successfully');
            
            logging('Image '.$this->id.' has been deleted');          
        }
        
        function canDeleteComment($id) {
            $user = $this->context->getCurrentUser();
            if ($user == null)
                return false;
            if ($user->IsAdmin)
                return true;
                
            $comment = $this->context->repository->getComment($id);
            return $comment->UserId == $user->Id;
        }
        
        function deleteComment($id) {
            logging('Trying to delete comment '.$id);
            
            if (!$this->canDeleteComment($id)) {
                logging('Can not delete comment '.$id);
            
                $this->error('You are not allowed to delete this comment!');
                return;
            }
            
            $this->context->repository->deleteComment($id);
            $this->setMessage('Comment successfully deleted.');
            
            logging('Successfully deleted comment '.$id);
        }
        
        function canDelete() {
            if ($this->img == null)
                return;
            
            $user = $this->context->getCurrentUser();
            if ($user == null)
                return false;
            if ($user->IsAdmin)
                return true;
            
            return $user->Id == $this->img->UserId;
        }
        
        function addComment() {
            logging('Trying to post new comment');
            
            if (!$this->canAddComment()) {
                logging('Comment could not be posted');
                $this->error('You must be logged in to post a comment');
                return;
            }
            
            $data = new CommentPostData;
            $data->parseRequest($_POST);
            
            if (!$data->isValid()) {
                logging('Comment could not be posted');
                $this->setError('Invalid Comment data');
                return;                
            }
            
            $comment = new Comment;
            $comment->Date = date('Y-m-d H:i:s');
            $comment->Content = $data->Content;
            $comment->UserId = $this->context->getCurrentUser()->Id;
            $comment->ImageId = $this->id;
            
            $this->context->repository->saveComment($comment);
            
            logging('Comment posted successfully');
        }
        
        function canAddComment() {
            return $this->context->isUserLoggedIn();
        }

        function createComment($comment) {
            $user = $this->context->repository->getUser($comment->UserId);
            
            $result = new CommentData;
            $result->Id = $comment->Id;
            $result->Content = htmlspecialchars($comment->Content);
            $result->UserId = $user->Id;
            $result->UserName = htmlspecialchars($user->Name);
            $result->Date = formatDate($comment->Date, 'm/d/Y H:i:s');
            $result->UserProfilePictureId = $user->ProfilePictureId;
            
            return $result;
        }
        
        function navigation() {
            if (!$this->canDelete())
                return;
            
            ?>
                <h2>Actions</h2>
                <form method="POST">
                    <ul>
                        <li>
                            <input type="submit" name="delete" value="Delete" />
                        </li>
                    </ul>
                </form>
            <?php
        }
        
        function help() {
            ?>
                <p>
                    This page displays a single image and its metadata. Click on the category to view images in this category or clik on the username to show the profile.
                </p>
                <p>
                    If logged in, you can comment a picture here.
                </p>
            <?php
        }
        
        function content() {
        ?>
            <h2 class="hidden"><?=htmlspecialchars($this->img->Title) ?></h2>
            
            <div class="image_imageArea">
                <img class="image_image" src="picture.php?id=<?=$this->image->PictureId ?>" alt="preview">
                
                <div class="image_metadataArea">
                    <h3 class="image_metadataHeading">Category:</h3>
                    <div class="image_metadata" >
                        <a href="bycategory.php?id=<?=$this->image->Category->Id?>"><?=htmlspecialchars($this->image->Category->Name) ?></a>
                    </div>
                    
                    <h3 class="image_metadataHeading">Date Taken:</h3>
                    <div class="image_metadata"><?=$this->image->DateTaken ?></div>
                    
                    <h3 class="image_metadataHeading">Photographer:</h3>
                    <div class="image_metadata">
                        <a href="profile.php?id=<?=$this->image->Photographer->Id?>"><?=htmlspecialchars($this->image->Photographer->Name) ?></a>
                    </div>
                    
                    <h3 class="image_metadataHeading">Date Uploaded:</h3>
                    <div class="image_metadata"><?=$this->image->DateUploaded ?></div>
                </div>
            </div>
            
            <h3>Comments</h3>
            <div class="image_commentArea">   
                <?php if ($this->canAddComment()) { ?>
                    <form class="image_commentForm" method="POST">
                        <label class="hidden" for="usercomment">Comment</label>
                        <textarea class="image_commentField" name="content" id ="usercomment" rows ="5" cols ="50"></textarea>
                        <input name="addComment" class="image_submitComment" type="submit" id="submitComment" value="Add comment" />
                    </form>
                <?php } ?>
                
                <?php if (count($this->image->Comments) > 0) { ?>
                    <ol class="image_comments">
                    <?php foreach ($this->image->Comments as $comment) { ?>
                        <li>
                            <div class="image_comment_user">
                                <img src="picture.php?id=<?=$comment->UserProfilePictureId ?>" alt="user" />
                                <a href="profile.php"><?=$comment->UserName ?>:</a>
                                <span><?=$comment->Date ?></span>
                            </div>
                            <p class="image_comment">
                                <?=$comment->Content ?>
                            </p>
                            <?php
                                if ($this->canDeleteComment($comment->Id)) { 
                            ?>
                                <form method="POST">
                                    <input type="hidden" name="commentId" value="<?=$comment->Id ?>" />
                                    <input type="submit" name="deleteComment" value="Delete" />
                                </form>
                          <?php
                                }
                            ?>
                        </li>
                    <?php } ?>
                    </ol>
                <?php } ?>
            </div>
        <?php
        }
    }
    
    $page = new ImageView();
    $page->build();
?>
