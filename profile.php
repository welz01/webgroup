<?php
    include('_common.php');
    
    class ProfileData extends Entity {
        function __construct($user = null) {
            $this->defineField('Title');
            $this->defineField('Name');
            $this->defineField('MemberSince');
            $this->defineField('Birthday');
            $this->defineField('EMail');
            $this->defineField('AboutMe');
            $this->defineField('ProfilePictureLink');
            $this->defineField('ProfileEditLink');
            $this->defineField('UserImagesLink');
            
            if ($user != null) {
                $this->Title = htmlspecialchars($user->Name).' (Profile)';
                $this->Name = htmlspecialchars($user->Name);
                $this->MemberSince = formatDate($user->MemberSince);
                $this->Birthday = formatDate($user->Birthday);
                $this->Name = htmlspecialchars($user->Name);
                $this->EMail = htmlspecialchars($user->EMail);
                $this->AboutMe = htmlspecialchars($user->AboutMe);
                $this->ProfilePictureLink = 'picture.php?id='.$user->ProfilePictureId;
                $this->ProfileEditLink = 'profile_edit.php?id='.$user->Id;
                $this->UserImagesLink = 'byuser.php?id='.$user->Id;
            }
        }
    }
    
    class Profile extends Template {
        private $data;
        
        function __construct() {
            parent::__construct();
            
            if (!$this->context->isUserLoggedIn()) {
                logging('Profile access denied');
                $this->error('You can only view profiles while logged in!');
                return;
            }
                        
            $userId = $this->context->getParameter('id', $this->context->getCurrentUser()->Id);
            $user = $this->context->repository->getUser($userId);
            
            logging('Accessing profile '.$userId);
            
            if ($user == null) {
                logging('User not found');
                $this->error('User not found');
                return;
            }
            
            $this->data = new ProfileData($user);
            $this->setTitle($this->data->Title);
        }
        
        function navigation() {
            if ($this->data == null)
                return;
            
            ?>
                <h2>Links</h2>
                <ul>                        
                    <li><a href="<?=$this->data->ProfileEditLink ?>">Edit profile</a></li>
                    <li><a href="<?=$this->data->UserImagesLink ?>">Images by this user</a></li>
                    <li><a href="upload.php">Photo Uploading</a></li>
                </ul>
            <?php
        }
        function help() {
            ?>
                <p>
                    This page allows you to view information about a specific user.
                </p>
                <p>
                    You can edit you own information here.
                </p>
            <?php
        }
        function content() { ?>
            
            <h2>General Information</h2>
            <div class="profilePicture">
                <img alt='profilePicture' src="<?= $this->data->ProfilePictureLink ?>" />
            </div> 
            
            <table class="profileData labelTable" title="user information">
                <tr>
                    <td>Name:</td>
                    <td><?=$this->data->Name ?></td>
                </tr>
                <tr>
                    <td>Member Since:</td>
                    <td><?=$this->data->MemberSince ?></td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td><?=$this->data->Birthday ?></td>
                </tr>
                <tr>
                    <td>E-Mail:</td>
                    <td><a href="mailto:<?=$this->data->EMail ?>"><?=$this->data->EMail ?></a></td>
                </tr>
            </table>
            
            <h2>About me</h2>
            <p class="aboutMe">
                <?=$this->data->AboutMe ?>
            </p>      
            <?php
        }
    }
    
    $page = new Profile();
    $page->build();
?>