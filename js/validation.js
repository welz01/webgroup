(function() {
    "use strict";
    
    $(document).ready(function() {
        $("*[data-validation-pattern], *[data-validation-match]").each(function() {
            var $this = $(this);
            var pattern = $this.attr("data-validation-pattern");
            if (!pattern)
                pattern = ".*";
            
            var match = $this.attr("data-validation-match");
            var message = $this.attr("data-validation-message");
            if (!message)
                message = "Invalid value!";
        
            var regex = new RegExp(pattern);
            
            $this.focus(function() {
                $this.data("entered", true);
                $this.css({
                    borderColor: "",
                });
            });
            $this.blur(function() {                
                var $next = $this.next();
                if ($next.is(".validationError"))
                    $next.remove();
                        
                var val = $this.val();
                if (val === "" && !$this.data("entered"))
                    return;
                
                var matches = true;
                if (match) {
                    var $element = $(match);
                    if ($element.val() !== val)
                        matches = false;
                }
                
                if (!regex.test(val) || !matches) {
                    $this.css({
                        borderColor: "red"
                    });
                    
                    var $span = $("<span></span>");
                    $span.addClass("validationError");
                    $span.text(message);
                    $span.insertAfter($this);
                }
            });
        }).blur();
    });
})();