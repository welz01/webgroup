<?php
    include('_common.php');
    
    class FullscreenData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('Number');
            $this->defineField('PictureLink');
            $this->defineField('PreviousLink');
            $this->defineField('NextLink');
            $this->defineField('ExitLink');
            $this->defineField('Title');
            
            $this->defineGetter('HasPrevious', 'hasPrevious');
            $this->defineGetter('HasNext', 'hasNext');
        }
        
        function hasPrevious() {
            return $this->PreviousLink != null;
        }
        function hasNext() {
            return $this->NextLink != null;
        }
    }
    
    abstract class ImageStrategy {
        protected $context;
        
        public function __construct($context) {
            $this->context = $context;
        }
        
        public abstract function loadImage($number);
        public abstract function makeLink($number); 
    }
    
    class LatestImageStrategy extends ImageStrategy {
        public function __construct($context) {
            parent::__construct($context);
        }
               
        public function loadImage($number) {
            $result = $this->context->repository->getLatestImages($number, 1);
            
            if ($result == null || count($result) == 0)
                return null;
            return $result[0];
        }
        public function makeLink($number) {
            return 'fullscreen.php?type=latest&amp;number='.$number;
        } 
    }
    
    class UserImageStrategy extends ImageStrategy {
        public function __construct($context) {
            parent::__construct($context);
            
            $this->userId = $context->getParameter('userId', null);
        }
        
        public function loadImage($number) {
            $result = $this->context->repository->getImagesByUser($this->userId, $number, 1);
            
            if ($result == null || count($result) == 0)
                return null;
            return $result[0];
        }
        public function makeLink($number) {
            return 'fullscreen.php?type=user&amp;userId='.$this->userId.'&number='.$number;
        } 
    }
    
    class CategoryImageStrategy extends ImageStrategy {
        public function __construct($context) {
            parent::__construct($context);
            
            $this->categoryId = $context->getParameter('categoryId', null);
        }
        
        public function loadImage($number) {
            $result = $this->context->repository->getImagesOfCategory($this->categoryId, $number, 1);
            
            if ($result == null || count($result) == 0)
                return null;
            return $result[0];
        }
        public function makeLink($number) {
            return 'fullscreen.php?type=category&amp;categoryId='.$this->categoryId.'&number='.$number;
        } 
    }
    
    class Fullscreen {
        private $data;
        private $context;
        
        private $number;
        
        function __construct() {
            $this->context = new Context;
            $this->strategy = $this->parseStrategy();
            $this->number = $this->context->getParameter('number', 0);
            
            $currentImage = $this->strategy->loadImage($this->number);
            
            $this->data = new FullscreenData();
            
            $this->data->Number = htmlspecialchars($this->number);
            $this->data->PictureLink = 'picture.php?id='.$currentImage->PictureId;
            $this->data->PreviousLink = $this->makeLink($this->number - 1);
            $this->data->NextLink = $this->makeLink($this->number + 1);
            $this->data->ExitLink = 'image.php?id='.$currentImage->Id;
            $this->data->Title = htmlspecialchars($currentImage->Title); 
        }
        
        function makeLink($number) {
            if ($number < 0)
                return null;
            if ($this->strategy->loadImage($number) == null)
                return null;
            
            return $this->strategy->makeLink($number);
        }
        
        function parseStrategy() {
            $type = $this->context->getParameter('type', 'latest');
                        
            if ($type == 'latest')
                return new LatestImageStrategy($this->context);
            if ($type == 'category')
                return new CategoryImageStrategy($this->context);
            if ($type == 'user')
                return new UserImageStrategy($this->context);
        
            return null;
        }
        
        function content() {
        ?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
                    <meta charset="utf-8" />
                    <link type="text/css" rel="stylesheet" href="css/page.css" />
                    
                    <title><?= $this->data->Title?> - CSUSM Photo Gallery</title>
            </head>
            <body class="fullscreen_body">
                <h1 class="hidden">Slideshow</h1>
                
                <div class="fullscreen_image">
                    <img alt="Fullscreen Image" src="<?=$this->data->PictureLink ?>" />
                </div>
                <div class="fullscreen_controls">
                <?php if ($this->data->HasPrevious) { ?>
                    <a class="previousLink" href="<?= $this->data->PreviousLink?>">Previous</a>
                <?php } ?>
                    <a class="exitLink" href="<?= $this->data->ExitLink?>">Exit</a>
                <?php if ($this->data->HasNext) { ?>
                    <a class="nextLink" href="<?= $this->data->NextLink ?>">Next</a>
                <?php } ?>
                </div>
            </body>
        <?php
        }
    }
    
    $page = new Fullscreen();
    $page->content();
?>

