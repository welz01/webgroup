<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link type="text/css" rel="stylesheet" href="css/page.css" />
	
	<title><?=$TEMPLATE->getTitle() ?> - CSUSM Photo Gallery</title>
        
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>
</head>
<body>
	<div id="header">
            <div id="greeting">
            <?php if ($TEMPLATE->context->isUserLoggedIn()) { ?>
                <span>Welcome <?= htmlspecialchars($TEMPLATE->context->getCurrentUser()->Name) ?>!</span>
                <form action="logout.php" method="POST" class="logoutForm">
                    <input type="submit" name="logout" value="Logout" />
                </form>
                <a href="profile.php?id=<?= $TEMPLATE->context->getCurrentUser()->Id ?>">Profile</a>
                <?php if ($TEMPLATE->context->getCurrentUser()->IsAdmin) { ?>
                    <a href="admin.php">Administration</a>
                <?php } ?>
            <?php
                }
                else {
            ?>
                <span>Not logged in</span>
                <a href="login.php">Login</a>
                <a href="register.php">Register</a>
            <?php
                }
            ?>    
            </div>
            <form class="searchform" action="search.php">
                <label for="searchText">Search: </label>
                <input type="text" name="searchText" id="searchText" />
                <input type="submit" name="search" id="search" value="Search" />
            </form>
            <h1>
                    <a href="index.php">
                            <img id="logo" src="http://www.ieconline.de/uploads/pics/CSUSM_logo.jpg" alt="CSUSM" />
                            <span>Photo Gallery - <?=$TEMPLATE->getTitle() ?></span>
                    </a>
            </h1>
	</div>
        
        <div id="sidebar">            
            <div id="navigation">
                <?=$TEMPLATE->getNavigation() ?>
            </div>
            
            <div id="help">
                <?=$TEMPLATE->getHelp() ?>
            </div>
        </div>
                 
	<div id="content">
            <?php if ($TEMPLATE->hasMessage()) { ?>
                <div id="message"><?=$TEMPLATE->getMessage() ?></div>
            <?php } ?>
        
            <?=$TEMPLATE->getContent() ?>
        </div>
</body>
</html>