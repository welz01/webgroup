<?php
    include('_common.php');
    
    class ByCategory extends Template {
        private $user;
        private $images; 
        private $category;
        private $categories;
        
        function __construct() {
            parent::__construct();
            
            $this->categories = $this->context->repository->getCategories();
            
            $categoryId = $this->context->getParameter('id', -1);
            $this->category = $this->context->repository->getCategory($categoryId);
            
            if ($this->category == null) {
                $this->images = array();
                $this->setTitle('Category');
            }
            else {
                $this->images = $this->context->repository->getImagesOfCategory($categoryId);
                $this->setTitle(htmlspecialchars($this->category->Name).' (Category)');
            }
            
            logging('Accessing category '.$categoryId);
        }
        
        function help() {
            ?>
            <p>
                This page shows pictures of a specific category. Click on a category to filter by it.
            </p>
            <?php
        }
        
        function content() {
            if ($this->category == null || count($this->images) == 0)
                return;
            
            photoList($this->images);
        }
        
        function navigation() {
        ?>
            <h2>Pick a category from below</h2>
            <ul>
                <?php
                    foreach ($this->categories as $category) {
                    ?>
                        <li>
                            <a href="bycategory.php?id=<?=$category->Id ?>"><?= htmlspecialchars($category->Name) ?></a>
                        </li>
                    <?php
                    }
                ?>
            </ul>
            
            <?php if ($this->category != null) { ?>
                <h2>Actions</h2>
                <ul>
                    <li><a href="fullscreen.php?type=category&amp;categoryId=<?=$this->category->Id ?>">Slideshow</a></li>
                </ul>
            <?php } ?>
        <?php
        }
    }
    
    $page = new ByCategory();
    $page->build();
?>