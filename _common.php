<?php
session_start();

include('_library.php');
include('_entities.php');

function getIpAddress() {
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    
    return $_SERVER['REMOTE_ADDR'];
}

function logging($message) {
    $ipAddress = getIpAddress();
    $file = $_SERVER['PHP_SELF'];
    
    $userId = 'N/A';
    if (isset($_SESSION['userId']))
        $userId = $_SESSION['userId'];
    
    $formattedMessage = "\n".date('Y-m-d H:i:s').' (UserId: '.$userId.', IP: '.$ipAddress.') - '.$file.': '.$message;
    
    $logfile = 'log/log.log';
    file_put_contents($logfile, $formattedMessage, FILE_APPEND | LOCK_EX);
}

function profilePicture($id) {
    $src = 'img/profile.png';
    if ($id)
        $src = "picture.php?id=$id";
        
    ?>
    <img alt="profile picture" src="<?=$src ?>" />
    <?php
}

function photoList($images) {
    if ($images == null || count($images) == 0)
        return;
    
    ?>
    <ul id="photos">
        <?php
            foreach ($images as $image) {
            ?>
                <li>
                    <a href="image.php?id=<?=$image->Id ?>">
                        <img class="thumbnailImage" alt="preview" src="picture.php?id=<?=$image->PictureId ?>" />
                        <span class="imageTitle"><?=htmlspecialchars($image->Title) ?></span>     
                    </a>
                </li>
            <?php
            }
        ?>
    </ul>
    <?php
}

class Context {
    public $repository;
    
    function __construct() {
        $this->repository = new Repository;
    }
    
    public function isPost() {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
    public function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }
    
    public function getCurrentUser() {
        if (!isset($_SESSION['userId']))
            return null;
            
        $id = $_SESSION['userId'];
        return $this->repository->getUser($id);
    }
    public function isUserLoggedIn() {
        return $this->getCurrentUser() != null;
    }
    public function getParameter($name, $default) {
        if(!array_key_exists($name, $_GET))
            return $default;
          
        return $_GET[$name];   
        //return filter_input(INPUT_GET, $name, FILTER_SANITIZE_STRING);
    }
}

abstract class Template {
    private $title;
    private $message;
    private $error;
    
    public $context;
    
    public function __construct() {
        $this->context = new Context();
    }    
    
    public function setTitle($title) {
        $this->title = $title;
    }
    public function navigation() {
    }
    public function help() {
    }
    public function content() {
    }
    
    public function hasMessage() {
        return $this->message !== null;
    }
    public function getNavigation() {
        ob_start();
        $this->navigation();
        $result = ob_get_clean();
        
        return $result;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function setError($error) {
        $this->setMessage('Error: '.$error);
    }
    public function error($message) {
        $this->error = $message;
        $this->setTitle('Error');
    }
    public function getMessage() {
        return $this->message;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getHelp() {
        ob_start();
        $this->help();
        $result = ob_get_clean();
        
        return $result;
    }
    public function getContent() {
        if ($this->error != null)
            return $this->error;
        
        ob_start();
        $this->content();
        $result = ob_get_clean();
        
        return $result;
    }
    
    public function build() {
        $TEMPLATE = $this;
        
        include('_layout.php');        
    }
}
?>