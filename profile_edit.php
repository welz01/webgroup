<?php
    include('_common.php');
    
    class EditProfileData extends Entity {
        function __construct($user = null) {
            $this->defineField('FirstName')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
            $this->defineField('LastName')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
            $this->defineField('Birthday')->addValidationRule(new DateValidationRule());
            $this->defineField('EMail')->addValidationRule(new EMailValidationRule());
            $this->defineField('AboutMe');
            $this->defineField('ProfilePictureLink');
            
            if ($user != null) {
                $this->FirstName = htmlspecialchars($user->FirstName);
                $this->LastName = htmlspecialchars($user->LastName);
                $this->Birthday = formatDate($user->Birthday);
                $this->EMail = htmlspecialchars($user->EMail);
                $this->AboutMe = htmlspecialchars($user->AboutMe);
                $this->ProfilePictureLink = 'picture.php?id='.$user->ProfilePictureId;
            }
        }
    }
    
    class EditProfile extends Template {
        private $userId;
        private $user;
        private $data;
        
        function __construct() {
            parent::__construct();
            
            $this->userId = $this->context->getParameter('id', $this->context->getCurrentUser()->Id);
            $this->user = $this->context->repository->getUser($this->userId);
            
            if ($this->user == null) {
                logging('User not found');
                $this->error('User not found');
                return;
            }
            if (!$this->hasPermissions()) {
                logging('Profile view denied');
                $this->error('You can only change your own profile!');
                return;
            }
            
            logging('Editing profile '.$this->userId);
            
            $this->data = new EditProfileData($this->user);            
            $this->processPostData();
            $this->setTitle('Edit Profile');
        }
        
        function hasPermissions() {
            $currentUser = $this->context->getCurrentUser();
            if ($currentUser == null)
                return false;
            if ($currentUser->IsAdmin)
                return true;
                
            return $currentUser->Id === $this->user->Id;
        }
        
        function processPostData() {
            if (!$this->context->isPost())
                return;
                
            logging('Received POST data');
            
            $data = new EditProfileData;
            $data->parseRequest($_POST);
            
            if(!$data->isValid()) {
                logging('POST data invalid');
                
                $this->setError('Please check the submitted values.');
                $this->data->copyFrom($data);
                return;
            }
            
            if (isset($_FILES['profilePicture']) && $_FILES['profilePicture']['size'] > 0) { 
                logging('Profile picture specified');
                               
                $tmpName  = $_FILES['profilePicture']['tmp_name'];  
                $img = file_get_contents($tmpName);
                
                $pictureId = $this->context->repository->storePicture($img);
                $this->user->ProfilePictureId = $pictureId; 
            }
            
            $this->user->FirstName = $data->FirstName;
            $this->user->LastName = $data->LastName;
            $this->user->Birthday = parseDate($data->Birthday);
            $this->user->EMail = $data->EMail;
            $this->user->AboutMe = $data->AboutMe;
            
            $this->context->repository->saveUser($this->user);
        
            $this->user = $this->context->repository->getUser($this->userId);
            $this->data = new EditProfileData($this->user);
            
            $this->setMessage('Changes saved successfully');
            logging('Profile updated successfully');
        }        
        function help() {
            ?>
                <p>
                    You can edit you own information here.
                </p>
            <?php
        }
        function content() {
            $user = $this->user;
            
            ?>
            <form  method="POST" enctype="multipart/form-data">
                <h2>General Information</h2>
                <div class="profilePicture">
                    <img alt='profile picture' src='<?= $this->data->ProfilePictureLink ?>' />
                </div>
                
                <table class="profileData labelTable" title="user information">
                    <tr>
                        <td>
                            <label for="firstName">First Name:</label>
                        </td>
                        <td>
                            <input id="firstName" name="firstName" class="profile_input" type="text" value="<?=$this->data->FirstName ?>" data-validation-pattern="^.{2,20}$" data-validation-message="Please enter a valid firstname" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lastName">Last Name:</label>
                        </td>
                        <td>
                            <input id="lastName" name="lastName" class="profile_input" type="text" value="<?=$this->data->LastName ?>" data-validation-pattern="^.{2,20}$" data-validation-message="Please enter a valid lastname"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="birthday">Birthday:</label>
                        </td>
                        <td>
                            <input id="birthday" name="birthday" class="profile_input" type="text" value="<?=$this->data->Birthday ?>" data-validation-pattern="^((((0[13578])|(1[02]))[\/]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\/]?(([0-2][0-9])|(30)))|(02[\/]?[0-2][0-9]))[\/]?\d{4}$" data-validation-message="Please enter a valid birthday." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="email">EMail:</label>
                        </td>
                        <td>
                            <input id="eMail" name="eMail" class="profile_input" type="text" value="<?=$this->data->EMail ?>" data-validation-pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$" data-validation-message="Please enter a valid mail address."/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="profilePicture">New profile picture:</label>
                        </td>
                        <td>
                            <input name="MAX_FILE_SIZE" value="1024000" type="hidden">
                            <input id="profilePicture" name="profilePicture" class="profile_input" type="file" />
                        </td>
                    </tr>
                </table>
                
                <h2><label for="aboutMe">About me</label></h2>
                <textarea id="aboutMe" name="aboutMe" class="aboutMe profile_textarea"><?=$this->data->AboutMe ?></textarea>
                
                <input class="profile_submit" type="submit" value="Save changes" />
            </form>  
            <?php
        }
    }
    
    $page = new EditProfile();
    $page->build();
?>