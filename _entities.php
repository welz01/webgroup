<?php

class User extends Entity {
    function __construct() {
        parent::__construct();
        
        $this->defineField('Id', 'UserId')->addValidationRule(new IntegerValidationRule());
        $this->defineField('EMail')->addValidationRule(new EMailValidationRule());
        $this->defineField('FirstName')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
        $this->defineField('LastName')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
        $this->defineField('MemberSince', 'MemberSince', new DateFormatter())->addValidationRule(new DateValidationRule());
        $this->defineField('Birthday', 'Birthday', new DateFormatter())->addValidationRule(new DateValidationRule());
        $this->defineField('AboutMe');
        $this->defineField('ProfilePictureId');
        $this->defineField('Password');        
        $this->defineField('IsActive');        
        $this->defineField('IsAdmin');    
        $this->defineField('ActivationKey');    
        
        $this->defineGetter('Name', 'getName');
        
        $this->Id = -1;
    }
    
    function getName() {
        return "$this->FirstName $this->LastName";
    }
}

class Image extends Entity {
    function __construct() {
        parent::__construct();
            
        //TODO: Validation
        $this->defineField('Id', 'ImageId');
        $this->defineField('Title');
        $this->defineField('DateTaken');
        $this->defineField('DateUploaded');
        $this->defineField('CategoryId');
        $this->defineField('UserId');
        $this->defineField('PictureId');
        
        $this->Id = -1;
    }
}

class Category extends Entity {
    function __construct() {
        parent::__construct();
            
        //TODO: Validation
        $this->defineField('Id', 'CategoryId');
        $this->defineField('Name');
        $this->defineField('Description');
        
        $this->Id = -1;
    }
}

class Comment extends Entity {
    function __construct() {
        parent::__construct();
            
        //TODO: Validation
        $this->defineField('Id', 'CommentId');
        $this->defineField('Content');
        $this->defineField('Date');
        $this->defineField('ImageId');
        $this->defineField('UserId');
        
        $this->Id = -1;
    }
}

function readUser($row) {
    $user = new User;
    $user->readRow($row);
    return $user;
}

function readCategory($row) {
    $category = new Category;
    $category->readRow($row);
    return $category;
}

function readImage($row) {
    $image = new Image;
    $image->readRow($row);
    return $image;
}

function readComment($row) {
    $comment = new Comment;
    $comment->readRow($row);
    return $comment;
}

class Repository {
    private $connection;
    
    function __construct() {
        $this->connection = new PDO('mysql:host=localhost;dbname=group6', 'group6', 'CA386gmb');
        //$this->connection = new PDO('mysql:host=localhost;dbname=welz01', 'welz01', 'welz01');

        $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        //$this->connection->exec('UPDATE Users SET IsAdmin = True WHERE UserID = 4');
        
        //TODO: Create if not existing
        //$this->recreateDb();
    }
    function __destruct() {
        $this->connection = null;
    }
    
    public function recreateDb() {
        //TODO: Read from file
        $this->connection->exec('DROP TABLE Comments');
        $this->connection->exec('DROP TABLE Images');
        $this->connection->exec('DROP TABLE Categories');
        $this->connection->exec('DROP TABLE Users');
        $this->connection->exec('DROP TABLE Pictures');
        
        $this->connection->exec('CREATE TABLE Pictures(PictureId SERIAL PRIMARY KEY NOT NULL, Data MEDIUMBLOB NOT NULL)');
        $this->connection->exec('CREATE TABLE Users(UserId SERIAL PRIMARY KEY NOT NULL, EMail Varchar(50) UNIQUE NOT NULL, Password Varchar(50) NOT NULL, FirstName Varchar(50) NOT NULL, LastName Varchar(50) NOT NULL, MemberSince DATETIME NOT NULL, Birthday Date, AboutMe Text, ProfilePictureId INTEGER NOT NULL, IsActive BOOLEAN NOT NULL, ActivationKey Char(40) NOT NULL, IsAdmin BOOLEAN NOT NULL, FOREIGN KEY (ProfilePictureId) REFERENCES Pictures(PictureId))');
        $this->connection->exec('CREATE TABLE Categories(CategoryId SERIAL PRIMARY KEY NOT NULL, Name Varchar(50) NOT NULL UNIQUE, Description Text)');
        $this->connection->exec('CREATE TABLE Images(ImageId SERIAL PRIMARY KEY NOT NULL, Title Varchar(50) NOT NULL, DateTaken Date, DateUploaded DATETIME NOT NULL, CategoryId INTEGER NOT NULL, UserId INTEGER NOT NULL, PictureId INTEGER NOT NULL, FOREIGN KEY (CategoryId) REFERENCES Categories(CategoryId), FOREIGN KEY (UserId) REFERENCES Users(UserId), FOREIGN KEY (PictureId) REFERENCES Pictures(PictureId))');
        $this->connection->exec('CREATE TABLE Comments(CommentId SERIAL PRIMARY KEY NOT NULL, Content Text NOT NULL, Date DateTime NOT NULL, ImageId Integer NOT NULL, UserId Integer NOT NULL, FOREIGN KEY (ImageId) REFERENCES Images(ImageId), FOREIGN KEY (UserId) REFERENCES Users(UserId))');
    }
    
    public function getUser($id) {
        $stmt = $this->connection->prepare('SELECT * FROM Users WHERE UserId = :id');
        $stmt->execute(array(':id' => $id));
        $row = $stmt->fetch();
        if (!$row)
            return null;
        
        return readUser($row);
    }
    
    public function findUsers($term) {
        $stmt = $this->connection->query('SELECT UserId, FirstName, LastName FROM Users WHERE FirstName LIKE :term OR LastName LIKE :term');
        $stmt->execute(array(':term' => $term));
        
        return array_map('readUser', $stmt->fetchAll());        
    }
    
    public function findCategories($term) {
        $stmt = $this->connection->query('SELECT * FROM Categories WHERE Name LIKE :term');
        $stmt->execute(array(':term' => $term));
        
        return array_map('readCategory', $stmt->fetchAll());        
    }
    
    public function saveComment($comment) {
        if (!$comment->isValid())
            throw new Exception('Invalid comment cannot be saved');
        
        $stmt = $this->connection->prepare('INSERT INTO Comments(Content, Date, ImageId, UserId) VALUES(:content, :date, :imageId, :userId)');
        $comment->setParameters($stmt);
        $stmt->execute();
    }
    
    public function getCommentsForPicture($id) {
        $stmt = $this->connection->prepare('SELECT * FROM Comments WHERE ImageId = :id');
        $stmt->execute(array(':id' => $id));
        
        return array_map('readComment', $stmt->fetchAll());
    }
    
    public function getComment($id) {
        $stmt = $this->connection->prepare('SELECT * FROM Comments WHERE CommentId = :id');
        $stmt->execute(array(':id' => $id));
        $row = $stmt->fetch();
        if (!$row)
            return null;
        
        return readComment($row);
    }
    
    public function getUsers() {            
        $stmt = $this->connection->query('SELECT UserId, EMail, FirstName, LastName, MemberSince, IsAdmin FROM Users');
        return array_map('readUser', $stmt->fetchAll());
    }
    
    public function findUserByEMail($eMail) {
        $stmt = $this->connection->query('SELECT * FROM Users WHERE EMail = :eMail');
        $stmt->execute(array(':eMail' => $eMail));
        
        $row = $stmt->fetch();
        if (!$row)
            return null;
            
        return readUser($row);
    }
    
    public function saveUser($user) {
        if (!$user->isValid())
            throw new Exception('Invalid user cannot be saved');
        
        $stmt = $this->connection ->prepare('UPDATE Users SET FirstName = :firstName, LastName = :lastName, Birthday = :birthday, EMail = :eMail, AboutMe = :aboutMe, Password = :password, ProfilePictureId = :profilePictureId, IsAdmin = :isAdmin WHERE UserId = :id');
        $user->setParameters($stmt);
        $stmt->execute();
    }
    
    public function createUser($user) {
        if (!$user->isValid())
            throw new Exception('Invalid user cannot be created');
        
        $stmt = $this->connection->prepare('INSERT INTO Users(FirstName, LastName, MemberSince, Password, EMail, IsActive, ActivationKey, IsAdmin) VALUES (:firstName, :lastName, :memberSince, :password, :eMail, :isActive, :activationKey, :isAdmin)');
        $user->setParameters($stmt);
        $stmt->execute();

        $id = $this->connection->lastInsertId();
        return $this->getUser($id);
    }
    
    public function createImage($image) {
        if (!$image->isValid())
            throw new Exception('Invalid image cannot be created');
        
        $stmt = $this->connection->prepare('INSERT INTO Images(Title, DateTaken, DateUploaded, CategoryId, UserId, PictureId) VALUES (:title, :dateTaken, :dateUploaded, :categoryId, :userId, :pictureId)');
        $image->setParameters($stmt);
        $stmt->execute();
        
        $id = $this->connection->lastInsertId();
        
        if ($image->Data != null) {
            $stmt = $this->connection->prepare('INSERT INTO ImageContents(ImageId, ImageData) VALUES (:imageId, :data)');
            $image->setParameters($stmt);
            $stmt->bindValue(':imageId', $id);
            $stmt->execute();
        }
        
        return $this->getImage($id);
    }
    
    public function findImages($term) {
        $stmt = $this->connection->query('SELECT * FROM Images WHERE Title LIKE :term');
        $stmt->execute(array(':term' => $term));
        
        return array_map('readImage', $stmt->fetchAll());        
    }
    
    public function getImage($id) {
        $stmt = $this->connection->prepare('SELECT * FROM Images WHERE ImageId = :id');
        $stmt->execute(array(':id' => $id));
        $row = $stmt->fetch();
        
        if (!$row)
            return null;
        
        return readImage($row);
    }
    
    public function deleteUser($id) {
        //Delete comments of user
        $stmt = $this->connection->prepare('DELETE FROM Comments WHERE UserId = :id');
        $stmt->execute(array(':id' => $id));
        
        //Delete Comments on images of user
        $stmt = $this->connection->prepare('DELETE FROM Comments WHERE ImageId IN (SELECT ImageId FROM Images WHERE UserId = :id)');
        $stmt->execute(array(':id' => $id));
        
        //Delete images of user
        $stmt = $this->connection->prepare('DELETE FROM Images WHERE UserId = :id');
        $stmt->execute(array(':id' => $id));
        
        //Delete User
        $stmt = $this->connection->prepare('DELETE FROM Users WHERE UserId = :id');
        $stmt->execute(array(':id' => $id));
    }
    
    public function deleteImage($id) {
        $stmt = $this->connection->prepare('DELETE FROM Comments WHERE ImageId = :id');
        $stmt->execute(array(':id' => $id));
        
        $stmt = $this->connection->prepare('DELETE FROM Images WHERE ImageId = :id');
        $stmt->execute(array(':id' => $id));
    }
    
    public function getImagesByUser($userId, $start = null, $limit = null) {
        if ($start == null)
            $start = 0;
        if ($limit == null)
            $limit = 10000;
        
        $stmt = $this->connection->prepare('SELECT * FROM Images WHERE UserId = :id LIMIT :start, :limit');
        $stmt->execute(array(':id' => $userId, ':start' => $start, 'limit' => $limit));
        
        return array_map('readImage', $stmt->fetchAll());
    }
    
    public function getImagesOfCategory($categoryId, $start = null, $limit = null) {
        if ($start == null)
            $start = 0;
        if ($limit == null)
            $limit = 10000;
            
        $stmt = $this->connection->prepare('SELECT * FROM Images WHERE CategoryId = :id LIMIT :start, :limit');
        $stmt->execute(array(':id' => $categoryId, ':start' => $start, 'limit' => $limit));
        
        return array_map('readImage', $stmt->fetchAll());
    }
    
    public function getLatestImages($start = null, $limit = null) {
        if ($start == null)
            $start = 0;
        if ($limit == null)
            $limit = 10000;
            
        $stmt = $this->connection->prepare('SELECT * FROM Images ORDER BY DateUploaded DESC LIMIT :start, :limit');
        $stmt->execute(array(':start' => $start, 'limit' => $limit));
        
        return array_map('readImage', $stmt->fetchAll());
    }
    
    public function storePicture($imageData) {
        $stmt = $this->connection ->prepare('INSERT INTO Pictures (Data) VALUES (:data)');
        $stmt->bindValue(':data', $imageData);
        $stmt->execute();        
        
        return $this->connection->lastInsertId();
    }
    
    public function deleteComment($id) {
        $stmt = $this->connection ->prepare('DELETE FROM Comments WHERE CommentId = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    
    public function deletePicture($id) {
        $stmt = $this->connection ->prepare('DELETE FROM Pictures WHERE PictureId = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    
    public function loadPicture($id) {
        $stmt = $this->connection ->prepare('SELECT Data FROM Pictures WHERE PictureId = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $row = $stmt->fetch();
        
        if (!$row)
            return null;
        
        return $row['Data'];
    }
    
    public function getCategory($id) {
        $stmt = $this->connection ->prepare('SELECT * FROM Categories WHERE CategoryId = :id');
        $stmt->execute(array(':id' => $id));
        $row = $stmt->fetch();
        if (!$row)
            return null;
        
        return readCategory($row);
    }
    
    public function getCategories() {            
        $stmt = $this->connection->query('SELECT * FROM Categories');
        return array_map('readCategory', $stmt->fetchAll());
    }
    
    function getOrCreateCategory($name) {        
        $stmt = $this->connection ->prepare('SELECT * FROM Categories WHERE Name = :name');
        $stmt->execute(array(':name' => $name));
        $row = $stmt->fetch();
        
        if (!$row) {
            $stmt->closeCursor();
            $stmt = $this->connection->prepare('INSERT INTO Categories(Name) VALUES (:name)');
            $stmt->execute(array(':name' => $name));
            
            $id = $this->connection->lastInsertId();
            return $this->getCategory($id);
        }
        
        return readCategory($row);
    }
}

?>