<?php
    include('_common.php');
    
    class LoginData extends Entity {
        function __construct() {
            parent::__construct();

            $this->defineField('EMail')->addValidationRule(new EMailValidationRule());
            $this->defineField('Password')->addValidationRule(new PatternValidationRule('#^.{6,}$#'));            
        }
    }
    
    class Login extends Template {
        function __construct() {
            parent::__construct();
            
            logging('Accessing login page');
            
            $this->setTitle('Login');
            $this->processPostData();
        }
        
        function processPostData() {
            if (!$this->context->isPost())
                return;
                
            logging('POST received');
            
            if (isset($_POST['submit']))
                $this->parseLogin();
            if (isset($_POST['forgotPassword']))
                $this->parseForgotPassword();
        }
        
        function parseLogin() {
            $data = new LoginData;
            $data->parseRequest($_POST);
            
            if (!$data->isValid()) {
                logging('Invalid login data received');
                $this->setError('Invalid login data');
                return;                
            }
            
            log('Login attempt for mail address '.$data->EMail);
            
            $user = $this->context->repository->findUserByEMail($data->EMail);
            if ($user == null) {
                logging('Login attempt for mail address '.$data->EMail.' failed: Wrong EMail address.');
            
                $this->setError('No user with this EMail address');
                return;
            }
            if (crypt($data->Password, $user->Password) === $user->Password) {                    
                logging('Login attempt for mail address '.$data->EMail.' successful.');
                
                $_SESSION['userId'] = $user->Id;
                $this->setMessage('Login successful');
                return;
            }
            
            session_destroy();
            logging('Login attempt for mail address '.$data->EMail.' failed: Wrong EMail password.');                    
            $this->setError('Wrong password');
        }
        
        function parseForgotPassword() {
            $data = new LoginData;
            $data->parseRequest($_POST);
            
            log('Forgotten password for mail address '.$data->EMail);
            
            $user = $this->context->repository->findUserByEMail($data->EMail);
            if ($user == null) {
                log('Password reset failed - unknown mail address '.$data->EMail);
            
                $this->setError('Could not reset password as email was not found');
                return;
            }
            
            $newPassword = generateRandomString(8);
            $user->Password = crypt($newPassword);
            $this->context->repository->saveUser($user);
            
            mail($user->EMail, 'CSUSM Photo Gallery - Password Reset', 'Hi '.$user->Name.",\nYou requested a reset of your password.\n\nYour new password is: ".$newPassword."\n\nRegards,\nThe CSUSM Photo Gallery Team");
            $this->setMessage('Your password has been reset successfully. Check your mailbox!');
            
            log('Password reset successful for mail address '.$data->EMail);
        }
        
        function help() {
            ?>
               <p>
                    Use this page to login. After logging in, you get access to personalized content.
                </p>
            <?php
        }
        
        function content() {
            ?>
            <p>
                Please login to gain full access to the CSUSM photo gallery.
            </p>
            <p>
                Don't have an account yet? <a href="register.php">Create one</a>.
            </p>
            <p>
                Forgot your password? Simply enter your email and click on "Forgot password?" to have it reset to an automatically generated one which will be sent to you via mail.
            </p>
            
            <form method="POST">
                <table class="labelTable">
                    <caption class="hidden">Login Table</caption>
                    <tr>
                        <td>
                            <label for="email">EMail:</label>
                        </td>
                        <td>
                            <input id="email" name="eMail" class="profile_input" type="text" data-validation-pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$" data-validation-message="Please enter a valid mail address."/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="password">Password:</label>
                        </td>
                        <td>
                            <input id="password" name="password" class="profile_input" type="password" data-validation-pattern="^.{6,}$" data-validation-message="Please check your password."/>
                        </td>
                    </tr>
                </table>
                
                <input id="submit" name="submit" type="submit" value="Login" />
                <input id="forgotPassword" name="forgotPassword" type="submit" value="Forgot password" />
            </form>
            <?php
        }
    }
    
    $page = new Login();
    $page->build();
?>