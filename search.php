<?php
    include('_common.php');
    
    class SearchResultData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('Title');
            $this->defineField('Users');
            $this->defineField('Categories');
            $this->defineField('Images');
            
            $this->defineGetter('HasUsers', 'hasUsers');
            $this->defineGetter('HasCategories', 'hasCategories');
            $this->defineGetter('HasImages', 'hasImages');
        }
        
        function hasUsers() {
            return count($this->Users) > 0;
        }
        function hasCategories() {
            return count($this->Categories) > 0;
        }
        function hasImages() {
            return count($this->Images) > 0;
        }
    }
    
    class UserSearchResultData extends Entity {
        function __construct($user = null) {
            parent::__construct();
            
            $this->defineField('ProfileLink');
            $this->defineField('Name');
            
            if ($user != null) {
                $this->Name = htmlspecialchars($user->Name);
                $this->ProfileLink = 'profile.php?id='.$user->Id;
            }
        }
    }
    
    class CategorySearchResultData extends Entity {
        function __construct($category = null) {
            parent::__construct();
            
            $this->defineField('Name');
            $this->defineField('Link');
            
            if ($category != null) {
                $this->Name = htmlspecialchars($category->Name);
                $this->Link = 'bycategory.php?id='.$category->Id;
            }
        }
    }
    
    class ImageSearchResultData extends Entity {
        function __construct($image = null) {
            parent::__construct();
            
            $this->defineField('Title');
            $this->defineField('PictureLink');
            $this->defineField('Link');
            
            if ($image != null) {
                $this->Title = htmlspecialchars($image->Title);
                $this->Link = 'image.php?id='.$image->Id;
                $this->PictureLink = 'picture.php?id='.$image->PictureId;
            }
        }
    }
    
    function mapUser($user) {
        return new UserSearchResultData($user);
    }
    
    function mapCategory($category) {
        return new CategorySearchResultData($category);
    }
    
    function mapImage($image) {
        return new ImageSearchResultData($image);
    }
    
    class Search extends Template {
        private $data;
        
        function __construct() {
            parent::__construct();
            
            $searchText = $this->context->getParameter('searchText', NULL);
            logging('Searched for '.$searchText);
            
            $this->data = new SearchResultData();
            $this->data->Title = 'Search results: \''.htmlspecialchars($searchText).'\'';
            
            $searchText = '%'.$searchText.'%';
            $this->data->Users = $this->searchUsers($searchText);
            $this->data->Categories = $this->searchCategories($searchText);
            $this->data->Images = $this->searchImages($searchText);
            
            $this->setTitle($this->data->Title);
        }
        
        function searchUsers($term) {
            $users = $this->context->repository->findUsers($term);
            return array_map('mapUser', $users);
        }
        
        function searchCategories($term) {
            $users = $this->context->repository->findCategories($term);
            return array_map('mapCategory', $users);
        }
        
        function searchImages($term) {
            $images = $this->context->repository->findImages($term);
            return array_map('mapImage', $images);
        }
        
        function help() {
            ?>
            <p>
                This page displays the search results.
            </p>
            <?php
        }
        
        function content() { ?>
            <h2>Users</h2> 
            <?php if ($this->data->HasUsers) { ?>
                <ul>
                <?php foreach ($this->data->Users as $user) { ?> 
                    <li><a href="<?=$user->ProfileLink ?>"><?=$user->Name ?></a></li>
                <?php } ?>
                </ul>
            <?php }
            else { ?>
                <p>No users found</p>
            <?php } ?>
            
            <h2>Categories</h2> 
            <?php if ($this->data->HasCategories) { ?>
                <ul>
                <?php foreach ($this->data->Categories as $category) { ?> 
                    <li><a href="<?=$category->Link ?>"><?=$category->Name ?></a></li>
                <?php } ?>
                </ul>
            <?php }
            else { ?>
                <p>No categories found</p>
            <?php } ?>
            
            <h2>Images</h2>
            <?php if ($this->data->HasImages) { ?>
                <ul id="photos">
                <?php foreach ($this->data->Images as $image) { ?>
                    <li>
                        <a href="<?=$image->Link ?>">
                            <img class="thumbnailImage" alt="preview" src="<?=$image->PictureLink ?>" />
                            <span class="imageTitle"><?=$image->Title ?></span>     
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php }
            else { ?>
                <p>No images found</p>
            <?php }
        }
    }
    
    $page = new Search();
    $page->build();
?>