﻿<?php
    include('_common.php');
    
    class UploadData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('Title')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
            $this->defineField('Category')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
            $this->defineField('Date')->addValidationRule(new DateValidationRule());
        }
    }
    class Upload extends Template {
        private $data;
        
        function __construct() {
            parent::__construct();
            
            logging('Accessing upload page');
            
            if (!$this->context->isUserLoggedIn()) {
                logging('Access to upload page denied');
                $this->error('You must be logged in to upload pictures!');
                return;
            }
            
            $this->setTitle('Upload');
            $this->processPostData();
        }
        
        function processPostData() {
            if (!$this->context->isPost())
                return;
                
            $this->data = new UploadData;
            $this->data->parseRequest($_POST);
            
            if (!$this->data->isValid()) {
                logging('Invalid POST data received');
                $this->setError('Invalid data');
                return;
            }
            if (!isset($_FILES['image']) || $_FILES['image']['size'] <= 0) {
                logging('No image received');                    
                $this->setError('Please specify a file');
                return;
            }
            if ($_FILES['image']['size'] > 972800) {
                logging('Image was too large');
                $this->setError('File is too large');
                return;
            }
            
            $image = new Image();                
            $image->Title = $this->data->Title;
            $image->DateTaken = parseDate($this->data->Date, 'm/d/Y');
            $image->DateUploaded = date('Y-m-d H:i:s');
            $image->UserId = $this->context->getCurrentUser()->Id;
            
            $imageData = file_get_contents($_FILES['image']['tmp_name']);
            $pictureId = $this->context->repository->storePicture($imageData);
            $image->PictureId = $pictureId;
            
            $category = $this->context->repository->getOrCreateCategory($this->data->Category);
            $image->CategoryId = $category->Id;
            
            $this->context->repository->createImage($image);
            
            logging('Image uploaded successfully');
            $this->setMessage('Image uploaded successfully');
        }
        
        function help() {
            ?>
               <p>
                    This page allows you to upload new photos.
                </p>
                <p>
                    Please note that the maximum file size is 900KB!
                </p>
            <?php
        }
        function content() {
            ?>            
            <form method="post" enctype="multipart/form-data">
                <table class="labelTable" title="Photo Information">
                    <tr>
                        <td><label for="title">Image Title:</label></td>
                        <td><input class="upload_input" name="title" type="text" id="title" data-validation-pattern="^.{2,20}$" data-validation-message="Please enter a valid title" /></td>
                    </tr>
                    <tr>
                        <td><label for="category">Category:</label></td>
                        <td><input class="upload_input" name="category" type="text" id="category" data-validation-pattern="^.{2,20}$" data-validation-message="Please enter a valid category" /></td>
                    </tr>
                    <tr>
                        <td><label for="date">Date Taken:</label></td>
                        <td><input class="upload_input" name="date" type="text" id="date" data-validation-pattern="^((((0[13578])|(1[02]))[\/]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\/]?(([0-2][0-9])|(30)))|(02[\/]?[0-2][0-9]))[\/]?\d{4}$" data-validation-message="Please enter a valid date (mm/dd/yyyy)." /></td>
                    </tr>
                    <tr>
                        <td><label for="image">File:</label></td>
                        <td><input class="upload_input" name="image" type="file" id="image" accept="image/*"/></td>
                    </tr>
                </table>
                
                <input type="hidden" name="MAX_FILE_SIZE" value="972800" />
                <input class="upload_submit" type="submit" name="submit" value="Upload" />
            </form>
            <?php
        }
    }
    
    $page = new Upload();
    $page->build();
?>