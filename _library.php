<?php

function lcfirst($value) {
    $value[0] = strtolower($value[0]);
    return $value;
}

function formatDate($date, $format = 'm/d/Y') {
    $d = strtotime($date);
    return date($format, $d);
}
function parseDate($date, $format = 'm/d/Y') {
    $d = strtotime($date);
    return date('Y-m-d', $d);
}

function generateRandomString($count) {
    $string = '';
    for ($i = 0; $i < $count; $i++)
        $string = $string.chr(mt_rand(33, 254));
        
     return base64_encode($string);
}

abstract class ValidationRule {
    public abstract function isValid($value);
}

class IntegerValidationRule extends ValidationRule {
    private $min;
    private $max;
    
    public function setMax($value) {
        $this->max = $value;
    }
    public function setMin($value) {
        $this->min = $value;
    }
    
    public function isValid($value) {
        if (!is_numeric($value))
            return false;
        if (isset($this->min) && $value < $this->min)
            return false;
        if (isset($this->max) && $value > $this->min)
            return false;
        if (floatval($value) != intval(floatval($value)))
            return false;
        
        return true;
    }
}

class NotEmptyValidationRule extends ValidationRule {
    public function isValid($value) {
        return $value != null && count($value) > 0;
    }
}

class EMailValidationRule extends PatternValidationRule {
    function __construct() {
        parent::__construct('#^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$#');
    }
}

class DateValidationRule extends ValidationRule {
    public function isValid($value) {
        return strtotime($value) !== FALSE;
    }
}

class PatternValidationRule extends ValidationRule {
    private $pattern;
    
    function __construct($pattern) {
        $this->pattern = $pattern;
    }
    
    public function isValid($value) {
        return preg_match($this->pattern, $value) != FALSE;
    }
}

abstract class EntityField {
    public abstract function getValue();
    public abstract function setValue($value);
    
    public abstract function readValue($row);
    public abstract function parseValue($request);
    
    public abstract function isValid();
}

class CallbackEntityField extends EntityField {
    private $functionName;
    private $owner;
    
    function __construct($owner, $functionName) {
        $this->functionName = $functionName; 
        $this->owner = $owner;
    }
    
    public function getValue() {
        $name = $this->functionName;
        $value = $this->owner->$name();
        return $value;
    }
    
    public function getDbValue() {
        return null;
    }
    public function setValue($value) {
    }
    public function readValue($row) {
    }
    public function parseValue($request) {
    }
    public function isValid() {
        return true;
    }
}

class ValueEntityField extends EntityField {
    private $name;
    private $value;
    private $column;
    private $validationRules;
    private $format;
    
    function __construct($name, $column = null, $format = null) {
        $this->name = $name;
        
        if ($column != null)
            $this->column = $column;
        else
            $this->column = $name;
            
        $this->validationRules = array();
        $this->format = $format;
    }
    
    public function addValidationRule($rule) {
        $this->validationRules[] = $rule;
        
        return $this;
    }
    
    public function isValidValue($value) {
        foreach ($this->validationRules as $rule) {
            if (!$rule->isValid($value))
                return false;
        }
        
        return true;
    }
    public function isValid() {
        return $this->isValidValue($this->getValue());
    }
    
    public function getDbValue() {
        return $this->value;
    }
    
    public function getValue() {
        if ($this->format !== null)
            return $this->format->formatValue($this->value);
        
        return $this->value;
    }
    
    public function setValue($value) {
        if ($this->format !== null)
            $this->value = $this->format->parseValue($value);
        else
            $this->value = $value;
    }
    
    public function readValue($row) {        
        if (array_key_exists($this->column, $row))
            $this->value = $row[$this->column];
    }
    
    public function parseValue($request) {
        $name = lcfirst($this->name);            
        
        if (array_key_exists($name, $request)) {
            $normalized = $request[$name];
            //$normalized = filter_var($normalized, FILTER_SANITIZE_STRING);
            $this->setValue($normalized);
        }
    }
}

abstract class ValueFormatter {
    public abstract function formatValue($value);
    public abstract function parseValue($value);
}

class DateFormatter extends ValueFormatter {
    public function formatValue($value) {
        return formatDate($value);
    }
    public function parseValue($value) {
        return parseDate($value);
    }
}

class Entity {
    private $fields;
    
    function __construct() {
        $this->fields = array();
    }
    
    protected function defineField($name, $column = null, $format = null) {
        $field = new ValueEntityField($name, $column, $format);
        $this->fields[$name] = $field;
        return $field;
    }
    protected function defineGetter($name, $functionName) {
        $field = new CallbackEntityField($this, $functionName);
        $this->fields[$name] = $field;
        return $field;
    }
    
    public function __get($property) {
        if (!array_key_exists($property, $this->fields))
            return null;
            
        return $this->fields[$property]->getValue();
    }
    public function __set($property, $value) {
        if (!array_key_exists($property, $this->fields))
            throw new Exception('Property '.$property.' doesn\'t exist');
        
        $this->fields[$property]->setValue($value);
        return $this;
    }
    
    public function isValid() {
        foreach ($this->fields as $field) {
            if (!$field->isValid())
                return false;
        }
        
        return true;            
    }
    public function readRow($row) {
        foreach ($this->fields as $field)
            $field->readValue($row);
            
        return $this;
    }
    public function parseRequest($request) {
        foreach ($this->fields as $field)
            $field->parseValue($request);
            
        return $this;            
    }
    public function copyFrom($other) {
        foreach ($this->fields as $name => $field) {
            $value = $other->$name;
            
            if ($value != null)
                $field->setValue($value);
        }
    }
    public function setParameters($stmt) {
        
        //TODO: Rewrite
        foreach ($this->fields as $name => $field) {
            $paramName = lcfirst($name); 
            $value = $field->getDbValue();
            
            if ($value === null)
                continue;
            
            try {
                $stmt->bindValue(":$paramName", $value);
            } catch (Exception $e) {
            }
        }
    }
}

?>