<?php
    include('_common.php');
    
    class Admin extends Template {
        private $users;
        private $categories;
        
        function __construct() {
            parent::__construct();
            
            logging('Administration page is being accessed');
            
            if (!$this->isAdmin()) {
                logging('Access to administration page denied');
                $this->error('This page is only available to administrators');
                return;
            }
            
            logging('Access to administration page granted');
            $this->processPostData();
            
            $this->users = $this->context->repository->getUsers();
            $this->categories = $this->context->repository->getCategories();
            
            $this->setTitle('Administration');
        }
        
        function processPostData() {
            if (!$this->context->isPost())
                return;
                
            if (isset($_POST['makeAdmin']))
                $this->makeAdmin($_POST['userId']);
            if (isset($_POST['unmakeAdmin']))
                $this->unmakeAdmin($_POST['userId']);
            if (isset($_POST['delete']))
                $this->deleteUser($_POST['userId']);
        }
        
        function makeAdmin($id) {
            $user = $this->context->repository->getUser($id);
            $user->IsAdmin = true;
            $this->context->repository->saveUser($user);
            
            $this->setMessage('User '.htmlspecialchars($user->Name).' has been made an Admin');
            logging('User '.htmlspecialchars($user->Name).' has been made an Admin');
        }
        
        function unmakeAdmin($id) {
            if ($id == $this->context->getCurrentUser()->Id) {
                $this->setError('You cannot revoke your own admin status!');
                return;
            }
            
            $user = $this->context->repository->getUser($id);
            $user->IsAdmin = false;
            $this->context->repository->saveUser($user);     
            
            $this->setMessage('User '.htmlspecialchars($user->Name).' has been revoked Admin rights');    
            logging('User '.htmlspecialchars($user->Name).' has been revoked Admin rights');       
        }
        
        function deleteUser($id) {
            $user = $this->context->repository->getUser($id);
            $this->context->repository->deleteUser($id);
            
            $this->setMessage('User '.htmlspecialchars($user->Name).' has been deleted');      
            logging('User '.htmlspecialchars($user->Name).' has been deleted');               
        }
        
        function help() {
            ?>
                <p>This page allows you to administer users.</p>
            <?php
        }
        
        function isAdmin() {
            $user = $this->context->getCurrentUser();
            if ($user == null)
                return false;
                
            return $user->IsAdmin;
        }
        
        function content() {
            ?>
            
            <h2>Users</h2>
            <table>
                <caption class="hidden">Users</caption>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>E-Mail</th>
                        <th>Name</th>
                        <th>Registration Date</th>
                        <th>Edit Profile</th>
                        <th>Delete User</th>
                        <th>Toggle Admin</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($this->users as $user) { ?>
                    <tr>
                        <td><a href="profile.php?id=<?=$user->Id ?>"><span class="hidden">User </span><?=$user->Id ?></a></td>
                        <td><a href="mailto:<?=$user->EMail ?>"><?=htmlspecialchars($user->EMail) ?></a></td>
                        <td><?=htmlspecialchars($user->Name) ?></td>
                        <td><?=$user->MemberSince ?></td>
                        <td><a href="profile_edit.php?id=<?=$user->Id ?>">Edit</a></td>
                        <td>
                            <form method="POST">
                                <input type="hidden" name="userId" value="<?=$user->Id ?>" />
                                <input type="submit" name="delete" value="Delete" />
                            </form>
                        </td>
                        <td>
                            <form method="POST">
                                <input type="hidden" name="userId" value="<?=$user->Id ?>" />
                                <?php if($user->IsAdmin) { ?>
                                    <input type="submit" name="unmakeAdmin" value="Unmake Admin" />
                                <?php } else { ?>
                                    <input type="submit" name="makeAdmin" value="Make Admin" />
                                <?php }?>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            
            <h2>Categories</h2>
            <table>
                <caption class="hidden">Categories</caption>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($this->categories as $category) { ?>
                    <tr>
                        <td><a href="bycategory.php?id=<?=$category->Id ?>"><span class="hidden">Category </span><?=$category->Id ?></a></td>
                        <td><?=htmlspecialchars($category->Name) ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php
        }
    }
    
    $page = new Admin();
    $page->build();
?>