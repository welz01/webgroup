<?php
    include('_common.php');
    
    class About extends Template {
        function __construct() {
            parent::__construct();
            
            $this->setTitle('Our Team');
        }
        
        function help() {
            ?>
                <p>This page gives you a brief overview over the team.</p>
            <?php
        }
        function content() {
            ?>
            <h2>Team Members</h2>
            <ul>
                <li>Valentin Malard</li>
                <li>Stéphane Etjemesian</li>
                <li>Martha Osorio</li>
                <li>Robert A Chiche</li>
                <li>Julian Baldner</li>
                <li>Matthias Welz</li>
            </ul>
            <?php
        }
    }
    
    $page = new About();
    $page->build();
?>