<?php
    include('_common.php');
    
    class ByUser extends Template {
        private $user;
        private $images; 
        
        function __construct() {
            parent::__construct();
            
            $userId = $this->context->getParameter('id', -1);
            $this->user = $this->context->repository->getUser($userId);
            
            if ($this->user == null) {
                $this->error('User not found');
                return;
            }
            
            $this->images = $this->context->repository->getImagesByUser($userId);
            $this->setTitle('Photos by '.htmlspecialchars($this->user->Name));
            
            logging('Accessing images by user '.$userId);
        }
        
        function navigation() {
            if ($this->user == null)
                return;
                                                                        
            ?>
                <h2>Actions</h2>
                <ul>
                    <li><a href="fullscreen.php?type=user&amp;userId=<?=$this->user->Id ?>">Slideshow</a></li>
                </ul>
            <?php
        }
        
        function help() {
            ?>
                <p>
                    This page shows pictures from a specific user.
                </p>
            <?php
        }
        
        function content() {
            photoList($this->images);
        }
    }
    
    $page = new ByUser();
    $page->build();
?>