<?php
    include('_common.php');
    
    class Index extends Template {
        function __construct() {
            parent::__construct();
            
            $this->setTitle('Index');
            $this->images = $this->context->repository->getLatestImages(0, 9);
            
            logging('Accessing front page');
        }
        
        function navigation() {
            ?>
                <h2>Links</h2>
                <ul>
                    <li><a href="about.php">About</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="register.php">Register</a></li>
                    <li><a href="bycategory.php">Category View</a></li>
                    <li><a href="upload.php">Photo Uploading</a></li>
                    <li><a href="fullscreen.php?type=latest">Slideshow</a></li>
                </ul>
            <?php
        }
        function help() {
            ?>
                <p>
                    This is the first page the user is going to see when entering the site. On the central left he will see various latest uploaded photos. On the top there will be the usual logo bar within an user managing box. On the right side of the page are links to several sub-sites.
                </p>
            <?php
        }
        function content() {
             ?>
            <h2>About this site</h2>
            <div>This is a photo sharing community site. Enjoy your stay.</div>

            <h2>Latest photos</h2>
            <?php
            photoList($this->images);
        }
    }
    
    $index = new Index;
    $index->build();
?>