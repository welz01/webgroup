<?php
    include('_common.php');
    $context = new Context();
    $id = $_GET['id'];

    header('Content-type: image/jpeg');
    $imageData = $context->repository->loadPicture($id);
    
    if ($imageData == null)
        $imageData = file_get_contents('./img/profile.png');

    echo $imageData;
?>