<?php
    include('_common.php');
    
    class RegisterUserData extends Entity {
        function __construct() {
            parent::__construct();
            
            $this->defineField('EMail')->addValidationRule(new EMailValidationRule());
            $this->defineField('FirstName')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
            $this->defineField('LastName')->addValidationRule(new PatternValidationRule('/^.{2,20}$/'));
            $this->defineField('Password')->addValidationRule(new PatternValidationRule('/^.{6,}$/'));
            $this->defineField('RepeatPassword')->addValidationRule(new PatternValidationRule('/^.{6,}$/'));
        }
    }
    
    class Register extends Template {
        private $user;
        
        function __construct() {
            parent::__construct();
            
            $this->user = new User;
            $this->setTitle('Register');
            
            $this->processPostData();
        }
        
        function processPostData() {
            if (!$this->context->isPost())
                return;
            
            $sentData = new RegisterUserData;
            $sentData->parseRequest($_POST);
            
            logging('Trying to register new user'.$this->user->Name.' ('.$this->user->EMail.')');
            
            if (!$sentData->isValid() || $sentData->Password != $sentData->RepeatPassword) {
                $this->setError('Invalid data');
                logging('Invalid data were posted to registration form.');
                return;
            }
            if ($this->context->repository->findUserByEMail($sentData->EMail) != null) {
                logging('EMail '.$this->user->EMail.' already in use');
                $this->setError('EMail already in use.');
                return;
            }
            
            $this->user->EMail = $sentData->EMail;
            $this->user->FirstName = $sentData->FirstName;
            $this->user->LastName = $sentData->LastName;
            
            $this->user->MemberSince = date('m/d/Y');
            $this->user->Password = crypt($sentData->Password);
            $this->user->IsActive = false;
            $this->user->ActivationKey = generateRandomString(30);
            
            $this->user = $this->context->repository->createUser($this->user);
            $_SESSION['userId'] = $this->user->Id;
            
            $this->setMessage('User created successfully');
            logging('User '.$this->user->Name.' successfully registered.');
        }
        
        function help() {
            ?>
                <p>
                    This page is for you to get an account. All you need to do is provide a Fullname, Email, and password.
                </p>
            <?php
        }
        
        function content() {
            ?>
            <p>
                Hello there! Once again welcome to Photo Sharing CSUSM!!! Please provide the information and click on register to start using this amazing website. If you have any questions please refer to our About Page from the homepage.
            </p>
            <p>
                Already have an account? Then continue to <a href="login.php">Login</a>.
            </p>
            <p>
                Please fill out all fields below:
            </p>
            
            <form method="POST">
                <table class="labelTable">
                    <caption class="hidden">
                        Register Table
                    </caption>
                    <tr>
                        <td>
                            <label for="firstName">First Name:</label>
                        </td>
                        <td>
                            <input id="firstName" name="firstName" class="profile_input" type="text" data-validation-pattern="^.{2,}$" data-validation-message="Please enter a valid first name. Ex: John">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lastName">Last Name:</label>
                        </td>
                        <td>
                            <input id="lastName" name="lastName" class="profile_input" type="text" data-validation-pattern="^.{2,}$" data-validation-message="Please enter a valid last name. Ex: Smith">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="eMail">EMail:</label>
                        </td>
                        <td>
                            <input id="eMail" name="eMail" class="profile_input" type="text" data-validation-pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$" data-validation-message="Please enter a valid email. Ex: josh001@mail.com"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="password">Password:</label>
                        </td>
                        <td>
                            <input id="password" name="password" class="profile_input" type="password" data-validation-pattern="^.{6,}$" data-validation-message="Please enter a valid password. Password must be at least 6 chars in length." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="repeatPassword">Repeat Password:</label>
                        </td>
                        <td>
                            <input id="repeatPassword" name="repeatPassword" class="profile_input" type="password" data-validation-match="#password" data-validation-message="Your passwords must match" />
                        </td>
                    </tr>
                </table>
                
                <input id="register" type="submit" value="Register" />
            </form>
            <?php
        }
    }
    
    $page = new Register();
    $page->build();
?>